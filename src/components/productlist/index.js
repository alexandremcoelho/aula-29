import { Component } from "react";
import { Sale } from "../sale";

export class ProductList extends Component {
  render() {
    const { name } = this.props.item;
    const { price } = this.props.item;
    const { discountPercentage } = this.props.item;
    return (
      <>
        <h1>{name}</h1>
        <b>{price}</b>
        <Sale>{discountPercentage}</Sale>
      </>
    );
  }
}
