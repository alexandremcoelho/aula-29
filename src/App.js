import logo from "./logo.svg";
import "./App.css";
import { Component } from "react";
import { ProductList } from "./components/productlist";

function App() {
  const productList = [
    {
      name: "Doce de abóbora",
      price: 0.5,
      discountPercentage: null,
    },
    {
      name: "Salgadinho",
      price: 2.5,
      discountPercentage: 10,
    },
    {
      name: "Refrigerante",
      price: 8.5,
      discountPercentage: 5,
    },
    {
      name: "Maçã",
      price: 0.7,
      discountPercentage: null,
    },
    {
      name: "Feijão",
      price: 2.7,
      discountPercentage: 15,
    },
  ];
  return (
    <div className="App">
      <header className="App-header">
        {productList.map((produto, index) => (
          <ProductList key={index} item={produto}></ProductList>
        ))}
      </header>
    </div>
  );
}

export default App;
